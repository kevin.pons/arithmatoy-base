# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return 'S' * n + '0'


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    return 'S' * (len(a) - 1) + b 


def multiplication(a: str, b: str) -> str:
    return 'S' * ((len(a) - 1) * (len(b) - 1)) + '0'


def facto_ite(n: int) -> int:
    if n == 0:
        return 1
    else:
        output = 1
        for k in range(2, n+1):
            output *= k
        return output


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n <= 1:
        return n
    else:
        return (fibo_rec(n - 1) + fibo_rec(n - 2))


def fibo_ite(n: int) -> int:
    a,b = 0,1
    for i in range(n):
        a,b = b,a+b
    return a


def golden_phi(n: int) -> int:
    return (fibo_rec(n + 1) / fibo_rec(n))


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    return a ** (n)
