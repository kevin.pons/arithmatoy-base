#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function");
  }
  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = (lhs_length > rhs_length ? lhs_length : rhs_length) + 1;
  char *result = malloc(result_length + 1);
  memset(result,'0', result_length);
  size_t carry = 0;
  unsigned int lhs_value;
  unsigned int rhs_value;
  unsigned int digit_sum=0;
  for (size_t i = 0; i < result_length; ++i) {
		lhs_value = (i < lhs_length ? get_digit_value(lhs[lhs_length - i - 1]) : 0);
		rhs_value = (i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0);
	 	if (VERBOSE) {
			if (i != result_length - 1){
			fprintf(stderr, "add: digit %c digit %c carry %d\n", lhs[lhs_length - i - 1], rhs[rhs_length - i - 1], carry);
	 		}
	 	}
 		digit_sum = lhs_value + rhs_value + carry;	 
 		if (i != result_length -1){
			carry = (digit_sum >= base ? 1 : 0);
		}
		digit_sum %=base;
		if (VERBOSE) {
			fprintf(stderr, "add: result: digit %c carry %d\n",to_digit(digit_sum), carry);
		}
		result[result_length - i - 1] = to_digit(digit_sum);
  }
	if (VERBOSE) {
		if (carry == 1) {
			fprintf(stderr, "add: final carry 1");
		}
	}
  const char *resultfinal = drop_leading_zeros(result);
  return resultfinal;

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Je récupère la taille de mes input
  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = lhs_length;
  /*if (strtol(lhs,NULL,base) < strtol(rhs,NULL,base)) {
	  return NULL;
  }*/
  //J'initialise ma variable de réponse
  char *result = malloc(result_length + 1);
  memset(result,'0',result_length);
  	
  //Initialisation de mes variables pour la boucle
  size_t carry = 0;
  unsigned int lhs_value;
  unsigned int rhs_value;
  int digit_sub = 0;
  char *reverse_lhs = lhs;
  char *reverse_rhs = rhs;
  reverse_lhs = reverse(reverse_lhs);
  reverse_rhs = reverse(reverse_rhs);
  //Pour chaque caractere remplissable de result
  for (size_t i = 0; i <= result_length; i++) {
	
 
	  // On converti la valeur en digit ou on attribue zero
	  lhs_value = (i < lhs_length ? get_digit_value(reverse_lhs[i]) : 0);
	  rhs_value = (i < rhs_length ? get_digit_value(reverse_rhs[i]) : 0);

	  // SI VERBOSE, ON AFFICHE LA PROCHAINE EXECUTION (sub: digit lhs digit rhs carry carry)
	  if (VERBOSE) {
		  fprintf(stderr,"sub: digit %c digit %c carry %d\n", reverse_lhs[i], reverse_rhs[i], carry);	  
	  }
	 // On fait le calcul pour avoir le result digit + son carry
	  digit_sub = lhs_value - rhs_value - carry;
	  if (digit_sub < 0) {
		  carry = 1;
		  digit_sub += base;
	  }
	  else {
		  carry = 0;
	  }

	  if (VERBOSE)
	  {
		  fprintf(stderr,"sub: result: digit %c carry %d\n",to_digit(digit_sub),carry);
		}
	result[i] = to_digit(digit_sub);
  }
  result = reverse(result);
  const char *resultfinal = drop_leading_zeros(result);
  if (carry == 0) {
  	return resultfinal;
  } else {
	  return NULL;
  }
  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }
  // Je récupère la taille de mes input (taille max = taille lhs + taille lhs)
  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = lhs_length + rhs_length;

  // J'initialise ma variable de réponse
  char *result = malloc(result_length+1);
  memset(result,'0',result_length);
  result[result_length] = '\0';
  unsigned int produit;
  size_t carry;
  int k;
  unsigned int digit;
  for (int i = rhs_length - 1; i >= 0; i--)
  {
	  for (int j = lhs_length - 1;j >= 0; j--)
	  {
		  produit = get_digit_value(lhs[j]) * get_digit_value(rhs[i]);
		  carry = 0;
		  k = i + j + 1;
		  if (VERBOSE) {
			  fprintf(stderr, "mul: digit %c digit %c carry %zu\n", lhs[j], rhs[i], carry); 
		  }
		  while (produit > 0 || carry > 0) {
			  digit = get_digit_value(result[k]) + carry + (produit % base);
			  carry = digit / base;
			  result[k] = to_digit(digit % base);
			  produit /= base;
			  k--;
		  }
		  if (VERBOSE) {
			  fprintf(stderr, "mul: result: digit %c carry %zu\n", result[k+1], carry);
		  }
	  }
  }

  return drop_leading_zeros(result);
  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  char *result = malloc(length+1);
  memset(result,'0',length);
  for (size_t i = 0; i < length; i++) {
    result[i] = str[length - i - 1];
  }
  result[length] = '\0';
  const char *resultfinal = result;
  return resultfinal;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
